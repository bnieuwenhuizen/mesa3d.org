---
title:    "May 13, 1999"
date:     1999-05-13 00:00:00
category: misc
tags:     []
---
For those interested in the integration of Mesa into XFree86 4.0,
Precision Insight has posted their lowlevel design documents at
[www.precisioninsight.com](http://www.precisioninsight.com).
