---
title:    "Mesa 20.0.5 is released"
date:     2020-04-22 00:00:00
category: releases
tags:     []
---
[Mesa 20.0.5](/relnotes/20.0.5.html) is released. This is a bug fix
release.
